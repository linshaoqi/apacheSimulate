package com.apache;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.Socket;

public class ProcessRequest extends Thread {

	private InputStream socketin;
	private OutputStream socketout;
	private PrintStream out;
	public final static String WEB_ROOT = "D:\\MyEclipse2015WorkSpace\\mvcTest\\WebRoot";

	public ProcessRequest(Socket socket) {
		try {
			this.socketin = socket.getInputStream();
			this.socketout = socket.getOutputStream();
			this.out = new PrintStream(this.socketout);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void run() {

		String filename = parseInfo(socketin);
		sendFile(filename);

	}

	private void sendError(int code, String errMessage) {
		
		out.println("HTTP/1.0 " + code + " " + errMessage);
		out.println("content-type: text/html");
		out.println();
		out.println("<html>");
		out.println("<head><title>Error Message</title></head>");
		out.println("<body>");
		out.println("<h1>errorcode:"+ code +" ErrorMessage:" + errMessage +"</h1>");
		out.println("</body>");
		out.println("</html>");
		out.flush();
		out.close();
	}

	private void sendFile(String filename) {
		if (null == filename) {
			return;
		}
		File file = new File(WEB_ROOT + filename);
		if (!file.exists()) {
			sendError(404, "File not Found!!!!");
			return;
		}
		byte[] content = new byte[(int) file.length()];
		try {
			InputStream in = new FileInputStream(file);
			in.read(content);
			out.println("HTTP/1.0 200 OK queryString");
			out.println("content-length:" + content.length);
			out.println();
			out.write(content);
			out.flush();
			out.close();
			in.close();
			socketin.close();
		} catch (IOException e) {
			sendError(500,"Server Error!!!!!");
			e.printStackTrace();
			
		}
	}

	private String parseInfo(InputStream inputStream) {
		BufferedReader br = new BufferedReader(new InputStreamReader(
				inputStream));
		try {
			String requestMessage = br.readLine();
			String[] content = requestMessage.split(" ");
			if (content.length != 3) {
				sendError(400, "client request error!!!");
				return null;
			}
			return content[1];
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}
}
