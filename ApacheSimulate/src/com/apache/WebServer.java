package com.apache;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class WebServer {
	
	private void startService(int port){
		try {
			ServerSocket serverSocket = new ServerSocket(port);
			while (true){
				Socket socket = serverSocket.accept();
				new ProcessRequest(socket).start();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	public static void main(String[] args) {
		int port = 80;
		if (args.length==1){
			port = Integer.parseInt(args[0]);
		}
		new WebServer().startService(port);
	}
}
